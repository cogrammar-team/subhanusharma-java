package com.techelevator;

public class Exercises {

	public static void main(String[] args) {

        /*
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch?
        */

		// ### EXAMPLE:
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;
		System.out.println(remainingNumberOfBirds);
        /*
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests?
        */

		// ### EXAMPLE:
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;
		System.out.println(numberOfExtraBirds);
        /*
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods?
        */
		int numberOfRacoons = 3;
		int goHome = 2;
		int racoonsLeft = numberOfRacoons - goHome;
		System.out.println(racoonsLeft);

        /*
        4. There are 5 flowers and 3 bees. How many less bees than flowers?
        */
		int flowers = 5;
		int bees = 3;
		int extraFlowers = flowers - bees;
		System.out.println(extraFlowers);
        /*
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now?
        */
		int pigeon = 1;
		int secondPigeon = 1;
		int totalPigeon = pigeon + secondPigeon;
		System.out.println(totalPigeon);

        /*
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now?
        */
        int owls = 3;
        int moreOwls = 2;
        int totalOwls = owls + moreOwls;
		System.out.println(totalOwls);

        /*
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home?
        */
		int workingBeavers = 2;
		int swimBeavers = 1;
		int remaingWorkingBeavers = workingBeavers - swimBeavers;
		System.out.println(remaingWorkingBeavers);

        /*
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all?
        */
        int toucans = 2;
        int joinToucan = 1;
        int totalToucan = toucans + joinToucan;
		System.out.println(totalToucan);

        /*
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts?
        */
        int squirrels = 4;
        int nuts = 2;
        int extraSquirrels = squirrels - nuts;
		System.out.println(extraSquirrels);

        /*
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find?
        */
        float quarter = 0.25f;
        float dime = 0.10f;
        float nickels = (float) (2 * 0.05);
        float totalMoney = quarter + dime + nickels;
		System.out.println(totalMoney);

        /*
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all?
        */

        int brierClassMuffins = 18;
		int bacAdamsClassMuffins = 20;
		int flanneryClassMuffins = 17;
		int totalMuffins = brierClassMuffins + bacAdamsClassMuffins + flanneryClassMuffins;

		System.out.println(totalMuffins);


        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		float yoyo = 0.24f;
		float whistle = 0.14f;
		float totalMoneySpent = yoyo + whistle;

		System.out.println(totalMoneySpent);


        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
        //int riceKrispie = 5;
        int marshmallows = 8;
        int miniMarshmallows = 10;

        int totalMarshmallow = marshmallows + miniMarshmallows;
		System.out.println(totalMarshmallow);
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
        int hiltSnow = 29;
        int brecknockSnow = 17;
        int hiltExtraSnow = hiltSnow - brecknockSnow;
		System.out.println(hiltExtraSnow);

        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
        int moneyHiltHas = 10;
        int costOfToyTruck = 3;
        int costOfPencil = 2;
        int remainingMoney= moneyHiltHas - costOfToyTruck - costOfPencil;

		System.out.println(remainingMoney);

        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
        int marbles = 16;
        int lostMarbles = 7;
        int remainingMarbles = marbles - lostMarbles;
		System.out.println(remainingMarbles);


        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int seashells = 19;
		int needForCollection = 25;
		int countRemainingSeashells = needForCollection - seashells;

		System.out.println(countRemainingSeashells);
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int redBallons = 8;
		int totalBallons = 17;
		int greenBallons = totalBallons - redBallons;
		System.out.println(greenBallons);
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
        int books = 38;
        int addedBooks = 10;
        int totalBooks = books + addedBooks;
		System.out.println(totalBooks);

        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int beeLegs = 6;
		int bee = 8;
		int countBeesLegs = bee * beeLegs;

		System.out.println(countBeesLegs);
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
        double icecream = 0.99;
        int qty = 2;
        double totalCostOfIcecream = icecream * qty;
		System.out.println(totalCostOfIcecream);

        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int totalRocksNeeded = 125;
		int rocksHiltHas = 64;
		int remainingRocks = totalRocksNeeded - rocksHiltHas;
		System.out.println(remainingRocks);

        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
        int hiltMarbles = 38;
        int hiltLostMarbles = 15;
        int remaingHiltMarbles = hiltMarbles - hiltLostMarbles;

		System.out.println(remaingHiltMarbles);



        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
        int totalMilesToDrive = 78;
        int stoppedForGas = 32;
        int remainingMiles = totalMilesToDrive - stoppedForGas;
		System.out.println(remainingMiles);

        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?

        */
        int totalTimeSaturdayMorning = 90;
        int totalTimeSaturdayAfternoon = 45;
        int totalTimeSpent = totalTimeSaturdayMorning + totalTimeSaturdayAfternoon;
		System.out.println("Total minutes spent:" + totalTimeSpent);

        /*
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
        int hotDogs = 6;
        double cost = 0.50;
        int totalCost = (int) (hotDogs * cost);
		System.out.println(totalCost);



        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
		double hiltMoney = 0.50;
		float pencil = (float)0.07;
		int countPencil = (int)(hiltMoney / pencil);
		System.out.println("Mrs. Hilt can buy " + countPencil + " pencils");

        /*
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
        int totalButterflies = 33;
       	int orangeButterflies = 20;
        int redButterflies = totalButterflies - orangeButterflies;
		System.out.println(redButterflies);

        /*
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
			double kateMoney = 1;
			double candyCost =  0.54;
			float remaingMoneyKate = (float)(kateMoney - candyCost);

			System.out.println("Kate gets back " + remaingMoneyKate + " cents");
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
        	int markTrees = 13;
        	int plantMoreTrees = 12;
        	int totalMarkTrees = markTrees + plantMoreTrees;

		System.out.println("Marks total trees " + totalMarkTrees);

        /*
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
         int days = 2;
         int hours = 24;
         int totalHours = days * hours;

		System.out.println("Total hours before Joy will see her grandma " + totalHours);

        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
        int cousins = 4;
        int gumsForEach = 5;
        int totalGumsNeeded = cousins * gumsForEach;

		System.out.println("Kim will need " + totalGumsNeeded + " gums");

        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
        	int danMoney = 3;
        	int costOfCandyBar = 1;
        	int danRemainingMoney = danMoney - costOfCandyBar;
		System.out.println("Dan has $" + danRemainingMoney + " left");

        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
        int boats = 5;
        int peopleOnEachBoat = 3;
        int peopleOnBoat = boats * peopleOnEachBoat;

		System.out.println("Total people on boat " + peopleOnBoat);

        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
        int legos = 380;
        int lost = 57;
        int remainingLegos = legos - lost;
		System.out.println(remainingLegos);

        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
        int bakedMuffins = 35;
        int totalMuffinsNeeded = 83;
        int totalMuffinsRemaingToBake = totalMuffinsNeeded - bakedMuffins;
		System.out.println("Arthur needs to bake " + totalMuffinsRemaingToBake + " muffins");

        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
        int willyCrayons = 1400;
        int lucyCrayons = 290;
        int willysExtraCrayons = willyCrayons - lucyCrayons;

		System.out.println("Willy has " + willysExtraCrayons + " crayons extra than Lucy");

        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
        int stickersOnPage = 10;
        int totalPages = 22;
        int totalStickers = stickersOnPage * totalPages;
		System.out.println("total stickers " + totalStickers);

        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
		int cupcakes = 96;
		int numberOfChildren = 8;
		int eachPersonCupcake = cupcakes / numberOfChildren;

		System.out.println("Each person gets " + eachPersonCupcake + " cupcakes");
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
		int gingerbread = 47;
		int gingerbreakInEachJar = 6;
		int notInJar = gingerbread % gingerbreakInEachJar;
		System.out.println("Cookies not in Jar " + notInJar);
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
        int croissants = 59;
        int numberOfNeighbors = 8;
        int remainingCroissants = croissants % numberOfNeighbors;
		System.out.println(remainingCroissants);

        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
        int cookiePerTray = 12;
        int totalCookieNeeded = 276;
        int totalTrayNeeded = totalCookieNeeded / cookiePerTray;
		System.out.println("Total trays needed " + totalTrayNeeded);

        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
        int pretzels = 480;
        int oneServing = 12;
        int totalServings = pretzels / oneServing;

		System.out.println("total servings " + totalServings);

        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
		int leamonCupcakes = 53;
		int cupcakeLeftAtHome = 2;
		int totalCupcakes = leamonCupcakes - cupcakeLeftAtHome;
		int eachBox = 3;
		int totalBoxes = totalCupcakes / eachBox;
		System.out.println(totalBoxes);


        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
        int carrot = 74;
        int people = 12;
        int uneatenCarrots = 74 % 12;
		System.out.println(uneatenCarrots);


        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
		int teddyBears = 98;
		int eachShelves = 7;
		int shelvesFilled = teddyBears / eachShelves;

		System.out.println(shelvesFilled);


        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
        int albumHasPictures = 20;
        int totalPictures = 480;
        int needAlbum = totalPictures / albumHasPictures;

		System.out.println(needAlbum);

        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
			int cards = 94;
			int boxHolds = 8;
			int unfilled = cards % boxHolds;
			int filled = cards / boxHolds;
		System.out.println("UnFilled " + unfilled);
		System.out.println("Filled " + filled);

        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
        int susieFatherBooks = 210;
        int shelves = 10;
        int eachShelfContains = susieFatherBooks / shelves;
		System.out.println(eachShelfContains);

        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
        int cristinaCroissants = 17;
        int guest = 7;
        int eachGuest =  cristinaCroissants / guest;
		System.out.println(eachGuest + " croissant");

        /*
            CHALLENGE PROBLEMS
        */

        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */

        int totalRooms = 5;
        double billHourlyRate = (12*14)/2.15;
        double jillHourlyRate = (12*14)/1.90;
        double totalHour = jillHourlyRate + billHourlyRate;
        double hoursNeeded = (totalRooms * 12*14)/(totalHour);
		System.out.println("Hours needed for 5 rooms " + hoursNeeded);
		totalRooms = 623;
		hoursNeeded = (totalRooms * 12*14)/(totalHour);
		System.out.println("Days " + hoursNeeded/8);

        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B."
        */
        String firstName = "Subhanu";
        String lastName = "Sharma";
        String middleInitial = "S.";
        String fullName = lastName + "," +  " " + firstName + " " + middleInitial;
		System.out.println(fullName);

        /*
        The distance between New York and Chicago is 800 miles, and the train has already travelled 537 miles.
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already travelled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
		double totalTripMiles = 800;
		double milesTraveled = 537;
		int percentageOfTravelledDistance = (int)((milesTraveled / totalTripMiles) * 100);
		System.out.println(percentageOfTravelledDistance + "%" + " of trip has been completed");
	}

}
