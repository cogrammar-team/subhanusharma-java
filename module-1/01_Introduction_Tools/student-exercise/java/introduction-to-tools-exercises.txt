----
1.  To begin, open your terminal, and enter the following command to change
    directory to the exercise directory.
----

$ cd ~/workspace/YOURNAME-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld

Note, do not enter the beginning "$" character. It represents the prompt for
the command line. This convention is commonly used to visually separate the
actual command from other text in Unix documentation.


----
2.  What does the ~ (tilde) character in the above command to change directory
    refer to?
----
indicates the home directories
<type your response here>

----
3.  A good habit to develop, especially while you're developing your skills on
    the command line, is to confirm your location after changing directories.
    This can be easily accomplished with the "pwd" command.
----

$ pwd
~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld (master)
<type or copy-paste the result here>

The present working directory displayed should match the directory you changed
to in Exercise 1.  Note, the two may not match at the very beginning if you
used the tilde when changing directories.  The tilde is a convenience to save
you some typing.  The system resolves the tilde to the full location as it
changes directory, and the "pwd" command reflects this.


----
4.  Display a list of all the files and directories in your present working
    directory. Include all dot-files and dot-directories.  Recall the "all" and
    "long format" flags for the "ls" command.
----

$ ls -la
total 8
drwxr-xr-x 1 Chase Java 197121    0 Sep  9 11:55 ./
drwxr-xr-x 1 Chase Java 197121    0 Sep  9 11:55 ../
drwxr-xr-x 1 Chase Java 197121    0 Sep  9 11:55 database/
-rw-r--r-- 1 Chase Java 197121 1130 Sep  9 11:55 pom.xml
drwxr-xr-x 1 Chase Java 197121    0 Sep  9 11:55 src/
<type or copy-paste the result here>


----
5.  What do the "." and ".." directories displayed in the directory listing in
    Exercise 4 represent?
----
cd . (displays the current directory)
cd .. takes you to the home directory

<type your answer here>

----
6.  What are the names of the files, including any dot-files, displayed in the
    directory listing in Exercise 4?
----
pom.xml
<type or copy-paste your answer here>


----
7.  What are the names of the directories, including any dot-directories, displayed in
    the directory listing in Exercise 4?
----
database
src

<type or copy-paste your answer here>


----
8.  pom.xml is a Maven build file. You will learn more about Maven in later
    lectures. As for the exercise, it is sometimes necessary, and usually
    always interesting to be able to view the contents of files while at the
    command line. A friendly and easy-to-use utility that allows you to do this
    is "less".

    "less" has two great features. First, it is a file viewer. You never have
    to worry about accidentally modifying the file, as you would if you opened it
    in an editor.  Second, it allows you to page-up, page-down, search, and
    perform other nifty navigations which are really useful when files are big.

    View pom.xml using "less".

    Or, for those who are more adventurous, you might want to try using "less" with a huge
    file like database/pagila.sql where the utility's navigation features really
    shine.

    Press Q to quit when you wish to stop viewing either of the files.
----

$ less pom.xml

Or

$ less database/pagila.sql

You are encouraged to make "less" part of your standard command line toolkit.

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
        <modelVersion>4.0.0</modelVersion>
        <groupId>com.techelevator</groupId>
        <artifactId>helloworld</artifactId>

<type or copy-paste the first five lines of either file here>

----
9.  This is good time to create a Git snapshot of your progress.  Usually,
    you'll want to check the Git status to review the list of files that
    have been modified, added, or deleted. Then you'll want to add and commit
    those changes to your local repository.

    Don't forget to save your changes to this text file before creating the snapshot.
----

Did you remember to save "introduction-to-tools-exercise.txt"?

$ git status

On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   ../introduction-to-tools-exercises.txt

no changes added to commit (use "git add" and/or "git commit -a")

<type or copy-paste the result here>

$ git add -A
<type or copy-paste the result here>
Chase Java@Chase-Java-V MINGW64 ~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld (master)

$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   ../introduction-to-tools-exercises.txt

$ git commit -m "Good stopping point"
<type or copy-paste the result here>

$ git commit -m "Good stopping point"
[master 5623588] Good stopping point
 1 file changed, 30 insertions(+), 1 deletion(-)

Note, you do not need to push this commit up to BitBucket at this time. The
typical Git workflow is to make many small commits to your local repo while
working. You push up to BitBucket when you're ready to share.

You'll be asked to push to BitBucket twice in this exercise, once a bit further
along, and then at the end.


----
10.  Change into src/main/java/com/techelevator directory under java-helloworld.
    Show your present working directory after the change.
----

$ cd src/main/java/com/techelevator
$ <enter command to display present working directory>
<type or copy-paste your present working directory here>
pwd 
~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/src/main/java/com/techelevator

----
11. Does the change directory command in Exercise 10 use a relative, or absolute
    path?
----
relative path
<type your answer here>


----
12. List all the files and directories, including any dot-files or dot-directories in
    your present working directory.
----

drwxr-xr-x 1 Chase Java 197121   0 Sep  9 11:55 ./
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 11:55 ../
-rw-r--r-- 1 Chase Java 197121 150 Sep  9 11:55 Helloworld.java

$ <enter command to list all files and directories including dot-files and dot-directories>
<type or copy-paste the result here>


----
13. "Helloworld.java" is the sole source code file of the Helloworld application.
    Make a backup of the file named "Helloworld.java.backup" using the copy
    command.
----

$ cp Helloworld.java Helloworld.java.backup

The "cp" parameters are the name of the source file followed by the name of the
destination file.

$ <enter command to list all files and directories in order to confirm the creation of the backup file>
<type or copy-paste the result here>

$ ls -al
total 2
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 15:00 ./
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 11:55 ../
-rw-r--r-- 1 Chase Java 197121 150 Sep  9 11:55 Helloworld.java
-rw-r--r-- 1 Chase Java 197121 150 Sep  9 15:00 Helloworld.java.backup


----
14. Rename "Helloworld.java.backup" to "Helloworld.java.copy" using the move
    command. (There is no rename command in UNIX.)
----

$ mv Helloworld.java.backup Helloworld.java.copy

The "mv" parameters are the existing name of the file followed by the new name
of the file.

$ <enter command to list all files and directories in order to confirm the renaming of the file>
<type or copy-paste the result here>

$ ls -al
total 2
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 15:02 ./
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 11:55 ../
-rw-r--r-- 1 Chase Java 197121 150 Sep  9 11:55 Helloworld.java
-rw-r--r-- 1 Chase Java 197121 150 Sep  9 15:00 Helloworld.java.copy

----
15. Let's call this a "feature complete" point in the exercises, and not only
    check-in to the local repository, but also push our work to our remote repository in
    BitBucket. Pushing our work to a remote repository is a great way to ensure that we
    don't accidentally lose work stored locally, and also enables us to share work with
    others on our teams.
----

$ <enter the Git command to display status>
<type or copy-paste the result here>
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   ../../../../../../introduction-to-tools-exercises.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Helloworld.java.copy

no changes added to commit (use "git add" and/or "git commit -a")


$ <enter the Git command to "stage" all changes>
<type or copy-paste the result here>
$ git add .
Chase Java@Chase-Java-V MINGW64 ~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/src/main/java/com/techelevator (master)

$ <enter the Git command to save all "staged" changes>
<type or copy-paste the result here>
$ git commit -m "new changes"
[master 058bf21] new changes
 2 files changed, 64 insertions(+), 2 deletions(-)
 create mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/src/main/java/com/techelevator/Helloworld.java.copy

$ git push origin master
<type or copy-paste the result here>
$ git push origin master
Enumerating objects: 32, done.
Counting objects: 100% (32/32), done.
Delta compression using up to 8 threads
Compressing objects: 100% (12/12), done.
Writing objects: 100% (20/20), 2.51 KiB | 367.00 KiB/s, done.
Total 20 (delta 5), reused 0 (delta 0)
To https://bitbucket.org/te-jpmc-tampa-2019/subhanusharma-java.git
   993b410..058bf21  master -> master

----
16. Delete the backup file, "Helloworld.java.copy" using the remove command.
----

$ rm Helloworld.java.copy

The "rm" parameter is the name of the file to be deleted.

$ <enter command to list all files and directories in order to confirm the file deletion>
<type or copy-paste the result here>
$ ls -al
total 1
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 15:11 ./
drwxr-xr-x 1 Chase Java 197121   0 Sep  9 11:55 ../
-rw-r--r-- 1 Chase Java 197121 150 Sep  9 11:55 Helloworld.java


----
17. Change directory to the java-helloworld directory using either one of the
    commands shown below.
----

$ cd ../../../../../

or

$ cd ~/workspace/YOURNAME-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld

Note the first alternative relies upon the ".." parent directory notation.  In
essence it says to change directory from the current directory's parent's parent's
parent's parent's parent.  You'll know you're getting comfortable at the
command line when that seems natural.

Regardless of the option you chose, check your new location.

$ <enter command to display present working directory>
<type or copy-paste your present working directory here>
/c/Users/Chase Java/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld

----
18. Create a documentation directory named "docs" under java-helloworld, and then
    change into it.
----

$ mkdir docs
$ <enter the command to change directory to "docs" using a relative path>
$ <enter command to display present working directory>
<type or copy-paste your present working directory here>
$ pwd
/c/Users/Chase Java/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs
----
19. Create a new file name "readme.txt" using Visual Studio Code, and add the text
    "Hello, this is the beginning of the application's documentation."
----

$ code readme.txt

Don't forget to save the file in Visual Studio Code!


----
20. As a final exercise, please perform the following commands to save and push your
    work so it can be reviewed by an instructor.
----

$ <enter the Git command to display status>
$ git add -A

Chase Java@Chase-Java-V MINGW64 ~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   ../../introduction-to-tools-exercises.txt
        new file:   readme.txt
        deleted:    ../src/main/java/com/techelevator/Helloworld.java.copy


$ <enter the Git command to "stage" all changes>
$ git add -A

Chase Java@Chase-Java-V MINGW64 ~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   ../../introduction-to-tools-exercises.txt
        new file:   readme.txt
        deleted:    ../src/main/java/com/techelevator/Helloworld.java.copy
$ git commit -m "changes"
[master 13a7caf] changes
 3 files changed, 24 insertions(+), 15 deletions(-)
 create mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs/readme.txt
 delete mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/src/main/java/com/techelevator/Helloworld.java.copy

$ <enter the Git command to save all "staged" changes>
$ git commit -m "changes"
[master 13a7caf] changes
 3 files changed, 24 insertions(+), 15 deletions(-)
 create mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs/readme.txt
 delete mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/src/main/java/com/techelevator/Helloworld.java.copy
$ <enter the Git command to save local repository to BitBucket>
git commit -m "changes"
[master 13a7caf] changes
 3 files changed, 24 insertions(+), 15 deletions(-)
 create mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs/readme.txt
 delete mode 100644 module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/src/main/java/com/techelevator/Helloworld.java.copy

Chase Java@Chase-Java-V MINGW64 ~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs (master)
$ ^C

Chase Java@Chase-Java-V MINGW64 ~/workspace/subhanusharma-java/module-1/01_Introduction_Tools/student-exercise/java/java-helloworld/docs (master)
$ git push origin master
Enumerating objects: 27, done.
Counting objects: 100% (27/27), done.
Delta compression using up to 8 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (15/15), 1.56 KiB | 266.00 KiB/s, done.
Total 15 (delta 2), reused 0 (delta 0)
To https://bitbucket.org/te-jpmc-tampa-2019/subhanusharma-java.git
   058bf21..13a7caf  master -> master

